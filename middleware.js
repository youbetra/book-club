const ExpressError = require('./utils/ExpressError');

module.exports.usernameToLowerCase = (req, res, next) => {
    req.body.username = req.body.username.toLowerCase();
    next();
}

module.exports.isLoggedIn = (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.json({ status: 401, message: "You must be signed in!" });
    }
    next();
}

module.exports.isBanned = (req, res, next) => {
    if (req.user && req.user.banned) {
        req.logout();
        return res.json({ status: 403, message: 'Your account has been banned.' });

    }
    next();
}

module.exports.isModerator = (req, res, next) => {
    if (req.user && !req.user.moderator) {
        return res.json({ status: 403, message: 'You are not a moderator.' });
    }
    next();
}

module.exports.isVerified = (req, res, next) => {
    if (req.user.emailVerified) {
        // email is verified
    } else {
        return res.json({ status: 403, message: 'You must verify your email first!' });
    }
    next();
}
