var fs = require('fs');

// function to encode file data to base64 encoded string
module.exports = function (file) {
    const encodedImage = fs.readFileSync(file, 'base64');
    return encodedImage;
}