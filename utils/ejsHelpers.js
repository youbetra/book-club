module.exports.getAttribute = (input, field) => {
    let output = [];
    for (let i = 0; i < input.length; ++i) {
        output.push(input[i][field]);
    }
    return output;
}

module.exports.getAssignedToCount = (booksInput, currentUser) => {
    const result = booksInput.filter((book) => book.assignee == currentUser.name);
    return result.length;
}

module.exports.combineTags = (booksInput) => {
    let output = [];
    let collected = [];
    let collectedCount = [];

    booksInput.forEach((book, bookIndex) => {
        book.tags.forEach((tag, tagIndex) => {
            let collectedIndex = collected.indexOf(tag);
            if (collectedIndex == -1) {
                collected.push(tag);
                collectedCount.push(1);
            } else {
                collectedCount[collectedIndex]++;
            }
        });
    });

    for (let i = 0; i < collected.length; i++) {
        let newObj = {
            tag: collected[i],
            count: collectedCount[i]
        }
        output.push(newObj);
    }
    output.sort((a, b) => a.count < b.count && 1 || -1)
    return output;
}

module.exports.timeSince = (date) => {
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = seconds / 31536000;
    if (interval > 1) {
        return Math.floor(interval) + " years ago";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
        return Math.floor(interval) + " months ago";
    }
    interval = seconds / 86400;
    if (interval > 1) {
        return Math.floor(interval) + " days ago";
    }
    interval = seconds / 3600;
    if (interval > 1) {
        return Math.floor(interval) + " hours ago";
    }
    interval = seconds / 60;
    if (interval > 1) {
        return Math.floor(interval) + " minutes ago";
    }
    return Math.floor(seconds) + " seconds ago";
}

module.exports.getCommentsLength = (comments) => {
    if (comments) {
        let length = comments.length;
        let replyLength = 0;
        comments.forEach((comment, index) => {
            if (comment.replies) {
                replyLength += comment.replies.length;
            }
        });
        return length + replyLength;
    } else {
        return 0;
    }
}

const isToday = (checkDate) => {
    const today = new Date()
    return checkDate.getDate() == today.getDate() &&
        checkDate.getMonth() == today.getMonth() &&
        checkDate.getFullYear() == today.getFullYear()
}

const isTodayRecurring = (checkDate, duration) => {
    const today = new Date()

    for (let i = 0; i < duration; i++) {
        let newDate = new Date(checkDate);
        newDate.setDate(newDate.getDate() + (7 * i));

        if (newDate.getDate() == today.getDate() &&
            newDate.getMonth() == today.getMonth() &&
            newDate.getFullYear() == today.getFullYear()) {
            return true;
        }

    }
    return false;
}

const isUpcoming = (checkDate) => {
    const today = new Date();
    const nextWeek = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7));

    if (nextWeek > checkDate && checkDate > today && !isToday(checkDate)) {
        return true;
    } else {
        return false;
    }
}

const isUpcomingRecurring = (checkDate, duration) => {
    const today = new Date()
    const nextWeek = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7));

    for (let i = 0; i < duration; i++) {
        let newDate = new Date(checkDate);
        newDate.setDate(newDate.getDate() + (7 * i));
        if (nextWeek > newDate && newDate > today && !isToday(newDate)) {
            return true;
        }

    }
    return false;
}

module.exports.getMeetingsToday = (meetings) => {
    let results = [];
    if (meetings) {
        meetings.forEach((meeting, index) => {
            if (isTodayRecurring(meeting.datetime, meeting.repeatduration)) {
                results.push(meeting)
            }
        });
        return results;
    } else {
        return results;
    }
}

module.exports.getMeetingsUpcoming = (meetings) => {
    let results = [];
    if (meetings) {
        meetings.forEach((meeting, index) => {
            if (isUpcomingRecurring(meeting.datetime, meeting.repeatduration)) {
                results.push(meeting)
            }
        });
        return results;
    } else {
        return results;
    }
}

module.exports.getFriendlyDate = (date) => {
    return date.toLocaleDateString('en-US');
}

module.exports.getFriendlyTime = (date) => {
    return date.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true }).replace(/^0+/, '');
}