const nodemailer = require("nodemailer");

module.exports.sendEmail = async function (messageObj) {
    // let testAccount = await nodemailer.createTestAccount();
    let transporter = nodemailer.createTransport({
        host: "smtp.office365.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS,
        },
        requireTLS: true,
        tls: {
            ciphers: 'SSLv3'
        }
    });
    let info = await transporter.sendMail({
        from: `"No Reply" <${process.env.SMTP_FROM}>`, // sender address
        to: messageObj.to, // list of receivers
        subject: messageObj.subject, // Subject line
        text: messageObj.bodyText, // plain text body
        html: messageObj.bodyHtml, // html body
    });
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

}