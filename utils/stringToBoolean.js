module.exports = function (string) {
    if (string) {
        switch (string.toLowerCase().trim()) {
            case "on": case "true": case "yes": case "1": return true;
            case "off": case "false": case "no": case "0": case null: return false;

            default: return Boolean(string);
        }
    } else {
        return false;
    }
}