const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MeetingSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    parentBook: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    body: String,
    subject: String,
    posted: Date,
    repeat: String,
    repeatduration: Number,
    datetime: Date,
    deleted: Boolean
});

const Meeting = mongoose.model('Meeting', MeetingSchema);

module.exports = { Meeting, MeetingSchema };