const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const ImageSchema = new Schema({
    url: String,
    filename: String
});

ImageSchema.virtual('thumbnail').get(function () {
    return this.url.replace('/upload', '/upload/w_200');
});

const UserSchema = new Schema({
    // username and password managed by passport module
    displayname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        collation: { locale: 'en', strength: 2 }
    },
    oAuthId: {
        type: String,
        unique: true,
        sparse: true,
        collation: { locale: 'en', strength: 2 }
    },
    image: ImageSchema,
    emailVerified: {
        type: Boolean,
        default: false
    },
    verificationToken: String,
    resetToken: String,
    resetTokenExp: Date,
    moderator: {
        type: Boolean,
        default: false
    },
    banned: {
        type: Boolean,
        default: false
    },
    books: [{
        type: Schema.Types.ObjectId,
        ref: 'Book'
    }],
    bookmarks: [{
        type: Schema.Types.ObjectId,
        ref: 'Book'
    }],
    clubs: [{
        type: Schema.Types.ObjectId,
        ref: 'Club'
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    notes: [{
        type: Schema.Types.ObjectId,
        ref: 'Note'
    }],
    meetings: [{
        type: Schema.Types.ObjectId,
        ref: 'Meeting'
    }],
    polls: [{
        type: Schema.Types.ObjectId,
        ref: 'Poll'
    }],
    alerts: [{
        type: Schema.Types.ObjectId,
        ref: 'Alert'
    }],
    settings: {
        darkmode: Boolean,
        reduceanims: Boolean, // removes the sliding and fading effects
        reduceeffects: Boolean, // removes the shadowing and glass effects
        alertpublic: Boolean,
        alerts: {
            comments: { type: Boolean, default: true },
            replies: { type: Boolean, default: true },
            meetings: { type: Boolean, default: true },
            polls: { type: Boolean, default: true },
            books: { type: Boolean, default: true },
            newmembers: { type: Boolean, default: true },
        }
    },
    deleted: Boolean
});

UserSchema.plugin(passportLocalMongoose);
UserSchema.statics.findOrCreate = function findOrCreate(profile, cb) {
    var userObj = new this();
    // this.findOne({ oAuthId: profile.id }, function (err, result) {
    this.findOne({
        $or: [
            { oAuthId: profile.id },
            { username: profile._json.email.toLowerCase() }
        ]
    }, function (err, result) {
        if (!result) {
            userObj.oAuthId = profile.id;
            userObj.username = profile._json.email.toLowerCase();
            userObj.email = profile._json.email.toLowerCase();
            userObj.emailVerified = profile._json.email_verified;
            userObj.displayname = profile.displayName;
            userObj.image = { url: profile._json.picture, filename: profile._json.picture };

            userObj.save(cb);
        } else {
            if (!result.oAuthId) {
                result.oAuthId = profile.id;
                result.save();
            }
            cb(err, result);
        }
    });
};

const User = mongoose.model('User', UserSchema);

module.exports = { User, UserSchema };