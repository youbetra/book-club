const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    body: String,
    replies: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    parentComment: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    parentBook: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    posted: Date,
    likes: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    updated: Date,
    siQuote: String,
    siPage: Number,
    deleted: Boolean
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = { Comment, CommentSchema };