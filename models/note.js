const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    parentBook: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    body: String,
    posted: Date,
    siQuote: String,
    siPage: Number,
    deleted: Boolean
});

const Note = mongoose.model('Note', NoteSchema);

module.exports = { Note, NoteSchema };