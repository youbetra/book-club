const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const slugify = require('../utils/slugify');

const ImageSchema = new Schema({
    url: String,
    filename: String,
});

const ClubSchema = new Schema({
    name: String,
    created: Date,
    color: String,
    description: String,
    icon: ImageSchema,
    private: Boolean,
    joinCode: String,
    owners: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    members: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    books: [{
        type: Schema.Types.ObjectId,
        ref: 'Book'
    }],
    deleted: Boolean
});
// virtuals
ClubSchema.virtual("slug").get(function () {
    return slugify(this.name);
});

const Club = mongoose.model('Club', ClubSchema);

module.exports = { Club, ClubSchema };