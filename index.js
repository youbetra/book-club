if (process.env.NODE_ENV !== "production") {
    require('dotenv').config();
}

const express = require('express');
const mongoose = require('mongoose');
const ejsMate = require('ejs-mate'); // allows layout templates in EJS
const session = require('express-session');
const flash = require('connect-flash');
const ExpressError = require('./utils/ExpressError');
const catchAsync = require('./utils/catchAsync');
const stringToBoolean = require('./utils/stringToBoolean');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const { User } = require('./models/user');
const { Book } = require('./models/book');
const { Club } = require('./models/club');
const { Comment } = require('./models/comment');
const { Note } = require('./models/note');
const { Meeting } = require('./models/meeting');
const { Poll } = require('./models/poll');


const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');

const usersRoutes = require('./routes/users');
const clubsRoutes = require('./routes/clubs');
const booksRoutes = require('./routes/books');
const commentsRoutes = require('./routes/comments');
const notesRoutes = require('./routes/notes');
const meetingsRoutes = require('./routes/meetings');
const pollsRoutes = require('./routes/polls');
const alertsRoutes = require('./routes/alerts');
const settingsRoutes = require('./routes/settings');
const { isBanned } = require('./middleware');

const methodOverride = require("method-override");
const cookieParser = require('cookie-parser');
const app = express();

// mongoose

const MongoDBStore = require("connect-mongo")(session);
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017/bookclub';

mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("DB connection opened.")
}).catch(e => {
    console.log("DB connection error: ", e);
});
const db = mongoose.connection;


// middleware
app.use(methodOverride('_method'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser(process.env.SECRET));
app.use(flash());

const path = require('path');
app.use(express.static(path.join(__dirname, 'public'))); //initialize static resources directory
app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

const ejsHelpers = require('./utils/ejsHelpers');
app.use((req, res, next) => {
    res.locals.ejsHelpers = ejsHelpers;
    res.locals.preload = false;
    next();
})

app.use((req, res, next) => {
    req.requestTime = new Date();
    next();
})

const secret = process.env.SECRET || 'E9!hY4bXRSkf-A3J!!';
const store = new MongoDBStore({
    url: dbUrl,
    secret,
    touchAfter: 24 * 60 * 60
});

store.on("error", function (e) {
    console.log("SESSION STORE ERROR", e)
})

const sessionConfig = {
    store,
    name: 'session',
    secret,
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        // secure: true,
        expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
        maxAge: 1000 * 60 * 60 * 24 * 7
    }
}


app.use(session(sessionConfig));
app.use(helmet());

const scriptSrcUrls = [
    "https://stackpath.bootstrapcdn.com/",
    "https://kit.fontawesome.com/",
    "https://fonts.googleapis.com/",
    "https://cdnjs.cloudflare.com/",
    "https://cdn.jsdelivr.net",
    "http://cdn.quilljs.com/"
];
const styleSrcUrls = [
    "https://kit-free.fontawesome.com/",
    "https://stackpath.bootstrapcdn.com/",
    "https://use.fontawesome.com/",
    "fonts.googleapis.com",
    "fonts.gstatic.com",
    "http://cdn.quilljs.com/"
];
const connectSrcUrls = [
    "fonts.googleapis.com",
    "fonts.gstatic.com",
    "https://openlibrary.org"
];
const fontSrcUrls = [
    "fonts.googleapis.com",
    "fonts.gstatic.com",
    "data:"
];
app.use(
    helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            connectSrc: ["'self'", ...connectSrcUrls],
            scriptSrc: ["'unsafe-inline'", "'self'", ...scriptSrcUrls],
            styleSrc: ["'self'", "'unsafe-inline'", ...styleSrcUrls],
            workerSrc: ["'self'", "blob:"],
            objectSrc: [],
            imgSrc: [
                "'self'",
                "blob:",
                "data:",
                "https://res.cloudinary.com/tetrayoubetra/", //SHOULD MATCH YOUR CLOUDINARY ACCOUNT! 
                "https://images.unsplash.com/",
                "https://upload.wikimedia.org/",
                "https://covers.openlibrary.org",
                "https://archive.org",
                "https://*.archive.org",
                "https://*.googleusercontent.com"
            ],
            fontSrc: ["'self'", "localhost", ...fontSrcUrls],
        },
    })
);

app.use(passport.initialize());
app.use(passport.session());
// local auth
passport.use(new LocalStrategy(User.authenticate()));

// google auth
passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "https://bookclub.tetrayoubetra.com/users/auth/google/callback"
},
    function (accessToken, refreshToken, profile, cb) {
        User.findOrCreate(profile, function (err, user) {
            return cb(err, user);
        });
    }
));
//
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(async (req, res, next) => {
    res.locals.currentNav = '';
    res.locals.currentUser = req.user;
    res.locals.success = req.flash('success');
    res.locals.error = req.flash('error');

    if (req.user) {
        let user = await User.findById(req.user._id).populate({
            path: 'clubs',
            match: {
                'deleted': {
                    $ne: true
                }
            }
        });
        res.locals.clubs = user.clubs;
    } else {
        res.locals.clubs = [];
    }

    next();
})

// check if banned
app.use(isBanned);

app.use(function (req, res, next) {
    res.renderWithData = function (view, model, data) {
        res.render(view, model, function (err, viewString) {
            data.view = viewString;
            res.json(data);
        });
    };
    next();
});

//////////////////////////////
// Testing - remove once sessions and users are implemented
// app.use((req, res, next) => {
//     res.locals.currentUser = { username: "tara@tetrayoubetra.com", name: "Tara Wilde" };
//     next();
// })
// simulate loading
app.use(async (req, res, next) => {
    // await new Promise(r => setTimeout(r, 5000));
    next();
});
//////////////////////////////

const navLinks = [
    {
        anchorTag: "Books",
        iconId: "booklogo",
        layout: "./layouts/books.ejs",
        preload: "./preload-layouts/books.ejs",
        onload: "Books"
    },
    {
        anchorTag: "Clubs",
        iconId: "grid",
        layout: "./layouts/clubs.ejs",
        preload: "./preload-layouts/clubs.ejs",
        onload: "Clubs"
    },
    {
        anchorTag: "Meetings",
        iconId: "calendar3",
        layout: "./layouts/meetings.ejs",
        preload: "./preload-layouts/meetings.ejs",
        onload: "Meetings"
    },
    {
        anchorTag: "Alerts",
        iconId: "bell",
        layout: "./layouts/alerts.ejs",
        preload: "./preload-layouts/alerts.ejs",
        onload: "Alerts"
    },
    {
        anchorTag: "Settings",
        iconId: "gear-fill",
        layout: "./layouts/settings.ejs",
        preload: "./preload-layouts/settings.ejs",
        onload: "Settings"
    }
];

const navLinksGuest = [
    {
        anchorTag: "Books",
        iconId: "booklogo",
        layout: "./layouts/books.ejs",
        preload: "./preload-layouts/books.ejs",
        onload: "Books"
    },
    {
        anchorTag: "Clubs",
        iconId: "grid",
        layout: "./layouts/clubs.ejs",
        preload: "./preload-layouts/clubs.ejs",
        onload: "Clubs"
    }
];


app.use('/users', usersRoutes);
app.use('/api/clubs', clubsRoutes);
app.use('/api/books', booksRoutes);
app.use('/api/comments', commentsRoutes);
app.use('/api/notes', notesRoutes);
app.use('/api/meetings', meetingsRoutes);
app.use('/api/polls', pollsRoutes);
app.use('/api/alerts', alertsRoutes);
app.use('/api/settings', settingsRoutes);

// app.get('/api/getComment/:bookId', async (req, res) => {
//     const { bookId } = req.params;
//     const { comment } = req.query;
//     const result = books.filter((book) => book.id == bookId);

//     // res.render('components/book-post', { expanded: true, books: result });
//     res.renderWithData('layouts-popout/book', { expanded: true, books: result }, { slug: slug });
// });

app.get('/', async (req, res) => {
    let links = navLinks;
    let alerts = [];
    if (!req.user) {
        links = navLinksGuest
    } else {
        let user = await User.findById(req.user._id).populate({
            path: 'alerts',
            match: {
                'read': {
                    $ne: true
                }
            }
        });
        user.alerts.forEach((alert, index) => {
            alerts.push(alert);
        });
    }
    res.render('app', { navLinks: links, alerts });
});

app.get('/:page*', async (req, res) => {
    const { page } = req.params;
    const data = req.body;
    let alerts = [];

    // console.log(req.query)  // filter query
    const foundpage = navLinks.filter((nav) => nav.anchorTag == page);
    if (foundpage.length > 0) {
        let links = navLinks;
        if (!req.user) {
            links = navLinksGuest
        } else {
            let user = await User.findById(req.user._id).populate({
                path: 'alerts',
                match: {
                    'read': {
                        $ne: true
                    }
                }
            });
            user.alerts.forEach((alert, index) => {
                alerts.push(alert);
            });
        }
        res.render('app', { navLinks: links, alerts });

    } else {
        res.redirect('/');
    }
});

// error handling middleware
app.all('*', (req, res, next) => {
    next(new ExpressError('Page Not Found', 404))
})

app.use((err, req, res, next) => {
    const { statusCode = 501 } = err;
    if (!err.message) err.message = 'Oh No, Something Went Wrong!'
    console.log(`Error at: ${req.originalUrl}`);
    console.log(`Method: ${req.method}`);
    console.log(err);
    res.status(statusCode).render('error', { err })
})

// port listeners always last
const PORT = process.env.PORT || 80;
app.listen(PORT, () => {
    console.log(`LISTENING ON PORT ${PORT}`)
});