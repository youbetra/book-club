
const quillOptions = {
    // debug: 'info',
    // modules: {
    //     toolbar: '#commenttoolbar'
    // },
    // placeholder: 'Make a post',
    readOnly: false,
    // scrollingContainer: true,
    theme: 'snow'
};
const commentEditor = new Quill('#commenttextarea', quillOptions);

const quillNewBookOptions = {
    // debug: 'info',
    // modules: {
    //     toolbar: '#commenttoolbar'
    // },
    placeholder: 'Description',
    readOnly: false,
    // scrollingContainer: true,
    theme: 'snow'
};
const newBookDescEditor = new Quill('#newbookdescription', quillNewBookOptions);
const newClubDescEditor = new Quill('#newclubdescription', quillNewBookOptions);

const commentInitial = `-${getComputedStyle(document.documentElement).getPropertyValue('--commentbar-thickness').trim()}`;

// run this when the page updates/loads to enable tooltips present on the views
function bootstrapInit() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
}
bootstrapInit();

let postType = "";
let postTo = "";
function showCommentInput(newButtonText, newPostType) {
    let buttonText = "Post a comment"
    postType = "Comment";
    if (newButtonText) {
        buttonText = newButtonText;
        postType = newPostType;
    }

    $('#commenttextarea').removeClass('d-none');
    $('#poll-container').addClass('d-none');
    $('#comment-popout .ql-toolbar').removeClass('d-none');

    switch (newPostType) {
        case "Meeting":
            $('#comment-siPage').addClass('d-none');
            $('#comment-siQuote').addClass('d-none');
            $('#meeting-datetime').removeClass('d-none');
            $('#meeting-controls').removeClass('d-none');
            $('#meeting-controls').addClass('d-flex');
            break;
        case "Poll":
            $('#poll-container').removeClass('d-none');
            $('#comment-popout .ql-toolbar').addClass('d-none');

            $('#commenttextarea').addClass('d-none');
            $('#meeting-controls').removeClass('d-flex');
            $('#meeting-controls').addClass('d-none');
            $('#comment-siPage').addClass('d-none');
            $('#comment-siQuote').addClass('d-none');
            $('#meeting-datetime').addClass('d-none');
            break;
        default: //Comment, Note 
            $('#comment-siPage').removeClass('d-none');
            $('#comment-siQuote').removeClass('d-none');
            $('#meeting-datetime').addClass('d-none');
            $('#meeting-controls').removeClass('d-flex');
            $('#meeting-controls').addClass('d-none');
            break;
    }
    $('#comment-popout .comment-collapse button').text(buttonText);
    $('#comment-popout').animate({ bottom: 0 }, 300).addClass("top-shadow");
}
function hideCommentInput() {
    $('#comment-popout').animate({ bottom: commentInitial }, 300, () => {
        $('#comment-popout').removeClass("top-shadow");
    })
}

function expandCommentInput(replyId, replyAuthor, editObj) {
    // showCommentInput();
    $('#comment-reply').removeData("replyid");
    $('#comment-reply').removeData("editid");
    $('.comment-expand').show();
    if (replyId) {
        if (editObj) {
            $('#comment-reply').html(`Editing your post`);
            $('#comment-reply').data("editid", replyId);
            $('#comment-reply').removeData("replyid");

            if (editObj.type == "Comment" || editObj.type == "Note") {
                $('#commenttextarea .ql-editor').html(editObj.body);
                $('#comment-siPage').val(editObj.siPage);
                $('#comment-siQuote').val(editObj.siQuote);
            } else if (editObj.type == "Meeting") {
                $('#meeting-subject').val(editObj.subject);
                $('#meeting-repeat').val(editObj.repeat);
                if (editObj.repeat == "weekly") {
                    $('#meeting-repeatduration').removeClass('d-none');
                    $('#meeting-repeatduration').val(editObj.repeatduration);
                } else {
                    $('#meeting-repeatduration').addClass('d-none');
                    $('#meeting-repeatduration').val(0);
                }
                $('#meeting-datetime').val(editObj.datetime);
                $('#commenttextarea .ql-editor').html(editObj.body);

            } else if (editObj.type == "Poll") {
                $('#poll-question').val(editObj.question);
                $('#poll-multiplechoice').prop("checked", editObj.multiplechoice);

                while ($('#poll-options .polloption').length < editObj.answers.length) {
                    $('#poll-options .polloption').first().clone().appendTo("#poll-options");
                    $('#poll-options .polloption').last().find('input').val("");
                }
                while ($('#poll-options .polloption').length > editObj.answers.length) {
                    $('#poll-options .polloption').last().remove();
                }
                const polloptions = $('#poll-options .polloption');
                editObj.answers.forEach((answer, index) => {
                    $(polloptions[index]).find('input').val(answer.answer);
                    $(polloptions[index]).data("_id", answer._id);
                });
            }
        } else {
            $('#comment-reply').html(`Replying to <strong>${replyAuthor}</strong>`);
            $('#comment-reply').data("replyid", replyId);
            $('#comment-reply').removeData("editid");
        }
        $('#comment-reply').show();
    }
    commentEditor.focus();
    $('.comment-collapse').hide();
}

function collapseCommentInput() {
    if ($('#comment-reply').data("replyid")) {
        showAlertMessage('error', "Reply discarded");
        clearCommentInput();
    }
    if ($('#comment-reply').data("editid")) {
        showAlertMessage('error', "Edit discarded");
        clearCommentInput();
    }
    commentEditPath = '';
    commentEditMethod = 'post';
    $('#comment-reply').removeData("replyid");
    $('#comment-reply').removeData("editid");

    $(".comment-expand .error").addClass("d-none");
    $('.comment-expand').hide();
    $('#comment-reply').hide();
    $('.comment-collapse').show();
}

function getPollAnswers() {
    let results = [];
    $('#poll-options .polloption').map(function (index, element) {
        results.push({ answer: $(element).find('input').val(), votes: [], id: $(element).data("_id") })
    })
    return results;
}

let commentEditPath = '';
let commentEditMethod = 'post'
function submitCommentInput() {
    $(".comment-expand .error").addClass("d-none");

    postTo = getTopBookPopout().dataset.id;
    let postToType = "Comment";

    if ($('#comment-reply').data("replyid")) {
        postTo = $('#comment-reply').data("replyid");
        postToType = "Reply";
    }
    const form = {
        'newcomment-parentBookId': getTopBookPopout().dataset.id,
        'newcomment-parentCommentId': postTo,
        'newcomment-postToType': postToType,
        'newcomment-body': commentEditor.root.innerHTML,
        'newcomment-siQuote': document.querySelector("#comment-siQuote").value,
        'newcomment-siPage': document.querySelector("#comment-siPage").value,
        // meetings
        'meeting-datetime': document.querySelector("#meeting-datetime").value,
        'meeting-subject': document.querySelector("#meeting-subject").value,
        'meeting-repeat': document.querySelector("#meeting-repeat").value,
        'meeting-repeatduration': document.querySelector("#meeting-repeatduration").value,
        // polls
        'poll-question': document.querySelector("#poll-question").value,
        'poll-multiplechoice': $(document.querySelector("#poll-multiplechoice")).prop('checked'),
        'poll-answers': getPollAnswers()
    };

    let path = '/api/comments';
    if (postType == "Note") {
        path = '/api/notes'
    } else if (postType == "Meeting") {
        path = '/api/meetings'
    } else if (postType == "Poll") {
        path = '/api/polls'
    }

    sendAjaxRequest(commentEditMethod, form, `${path}${commentEditPath}`, (res) => {
        if (res.status.toString().charAt(0) == "2") {

            showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id, `${postType}s`, res.newid);
            clearCommentInput();
            collapseCommentInput();
        } else {
            showAlertMessage('error', res.message);
        }
    });
}

function clearCommentInput() {
    commentEditor.setText('');
    $("#comment-popout").find("input").val("");
    $("#meeting-repeatduration").val(0);
    $('#comment-reply').removeData("replyid");
    $('#comment-reply').removeData("editid");
    $('#commenttextarea .ql-editor').html('');
}

$('#meeting-repeat').on("change", function (e) {
    if ($(e.target).val() == "weekly") {
        $('#meeting-repeatduration').removeClass('d-none');
        $('#meeting-repeatduration').val(0);
    } else {
        $('#meeting-repeatduration').addClass('d-none');
    }
});

let KeyDown;
let ClickY
$('#comment-popout').on("touchstart mousedown", function (e) {
    if ($(e.target).is('div')) {
        e.preventDefault();
        KeyDown = true;
        ClickY = e.pageY;
        expandCommentInput();
    }
});
$('body').on("touchend mouseup", function (e) {
    KeyDown = false;
});
$(document).on("mousemove touchmove", function (e) {
    if (KeyDown) {
        e.preventDefault();
        let offset = ClickY - e.pageY;
        let newH = $('#comment-popout .comment-expand #commenttextarea').height() + offset;

        $('#comment-popout .comment-expand #commenttextarea').height(newH);

        ClickY = e.pageY;
    }
});

// determine if clicked outside of comment popout
let mouse_is_inside = false;
$('#comment-popout').hover(function () {
    mouse_is_inside = true;
}, function () {
    mouse_is_inside = false;
});

$("body").on("mousedown touchstart", function (e) {
    var element = document.getElementById('comment-popout');

    if (e.target !== element && !element.contains(e.target)) {
        collapseCommentInput();
    }

    if (!mouse_is_inside && !KeyDown) {
        // collapseCommentInput();
    };
});

collapseCommentInput();

function addPollOption() {
    if ($('#poll-options .polloption').length < 8) {
        $('#poll-options .polloption').first().clone().appendTo("#poll-options");
        $('#poll-options .polloption').last().find('input').val("");
    }
}

function removePollOption(element) {
    if ($('#poll-options .polloption').length > 1) {
        $(element).parent().remove();
    }
}

// new book popout
function showNewBookPopout() {
    $("#search-spinner").hide();
    $("#loadmore-link").hide();
    $("#search-result-_template").hide();
    $("#postbook-pane").hide();
    $("#resultbook-pane").show();
    $("#popout-newbook").animate({ right: 0 }, 300, () => {

    });
}
function hideNewBookPopout() {
    const currentPopouts = $(".content-popout");
    if (currentPopouts.length <= 3) {
        homeClubId = "allclubs";
        selectClubById(homeClubId);
    }
    if (newbookEndpoint != '/api/books') {
        showAlertMessage('error', "Edit discarded");
    }
    newbookEndpoint = '/api/books';
    newbookMethod = 'post';

    clearFileInputs();

    $("#popout-newbook").animate({ right: "-100vw" }, 300, () => {

    });
}

let searchKey = "title";
function changeNewBookSearch(newSearchKey, elem) {
    searchKey = newSearchKey;
    $("#searchBy").text($(elem).text());
}

function buildBookResult(loadPage, maxLength) {
    for (let i = loadPage; i < Math.min(Math.max(searchResults.length, 0), loadPage + maxLength); i++) {
        const book = searchResults[i];
        const bookKey = book.key.replaceAll("/", "");

        let title = (book.title) ? book.title : "Title N/A";
        let authors = "Author N/A"
        let published = (book.first_publish_year) ? book.first_publish_year : "Publish date N/A";
        if (book.author_name) {
            authors = book.author_name.toString();
            authors = authors.replaceAll(',', ', ');
        }

        const newResultElem = $('#search-result-_template').clone();
        newResultElem[0].id = newResultElem[0].id.replace("_template", bookKey);
        newResultElem.appendTo($('#search-results'));
        newResultElem.fadeIn(2000);
        const editElem = document.querySelector(`#${newResultElem[0].id}`);
        editElem.innerHTML = editElem.innerHTML.replaceAll("_template", bookKey);

        editElem.querySelector("img").src = (book.cover_edition_key) ? `https://covers.openlibrary.org/b/olid/${book.cover_edition_key}-L.jpg` : '/img/book-placeholder.jpg';
        let aId;
        if (book.ia_loaded_id || book.ia) {
            aId = (book.ia_loaded_id) ? book.ia_loaded_id[0] : book.ia[0];
            editElem.querySelector(".btn-book-popout").onclick = function () { openArchivePage(aId) };
            editElem.querySelector(".btn-book-popout").classList.add("cursor-pointer");
            editElem.querySelector(".btn-book-popout").classList.remove("d-none");
        }
        editElem.querySelector(".result-title").innerText = title;

        editElem.querySelector(".result-author").innerText = authors;
        editElem.querySelector(".result-published").innerText = published;
        editElem.classList.add("search-result");

        editElem.dataset.olId = book.cover_edition_key;
        editElem.dataset.aId = aId;
        editElem.dataset.title = title;
        editElem.dataset.published = published;
        editElem.dataset.author = authors;
    }
}

let searchResults = [];
let page = 0;
let maxResults = 20;
function sendSearch() {
    let searchText = $("#search-input").val();
    //https://openlibrary.org/search.json?title=Dune
    //http://openlibrary.org/api/books?bibkeys=ISBN:0201558025
    config = {};
    if (searchText) {
        $("#loadmore-link").hide();

        $("#search-spinner").show();
        $(".search-result").remove();
        axios.get(`https://openlibrary.org/search.json?${searchKey}=${searchText}`, config).then(function (res) {
            if (res.status.toString().charAt(0) == "2") {
                $("#search-spinner").hide();
                const maxResults = 12;
                searchResults = res.data.docs;
                buildBookResult(0, maxResults);
                $("#loadmore-link").show();
                $("#loadmore-link a").show();
                $("#result-counter").text(`${searchResults.length} matches found!`)
            }
        });
    }
}
$("#newbook-search-form").submit((e) => {
    e.preventDefault();
    sendSearch();
});

let newbookEndpoint = '/api/books';
let newbookMethod = 'post';
$("#form-newbook").submit((e) => {
    e.preventDefault();
    $("#popout-newbook-flash").text("");
    $("#popout-newbook-flash").addClass('d-none');

    let formData = new FormData(document.querySelector("#form-newbook"));
    formData.append("newbook-description", newBookDescEditor.root.innerHTML);
    formData.append("newbook-olId", document.querySelector("#newbookolId").value);
    formData.append("newbook-aId", document.querySelector("#newbookaId").value);
    formData.append("newbook-clubId", selectedClubId);
    formData.append("newbook-altCover", $("#newbookImg").attr("src"));

    sendAjaxImageRequest(newbookMethod, formData, newbookEndpoint, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            window.location.href = `/Books/${res.data.bookid}`;
        } else {
            showAlertMessage('error', res.message)
            // $("#popout-newbook-flash").text(res.message);
            // $("#popout-newbook-flash").removeClass('d-none');
        }
    });

});

let newclubEndpoint = '/api/clubs';
let newclubMethod = 'post';
$("#form-newclub").submit((e) => {
    e.preventDefault();

    let formData = new FormData(document.querySelector("#form-newclub"));
    formData.append("newclub-description", newClubDescEditor.root.innerHTML.replaceAll("<p>", "<p class='text-center'>"));

    sendAjaxImageRequest(newclubMethod, formData, newclubEndpoint, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            window.location.href = `/Clubs/${res.data.clubid}`;
        } else {
            showAlertMessage('error', res.message)
            // $("#popout-newclub-flash").text(res.message);
            // $("#popout-newclub-flash").removeClass('d-none');
        }
    });
});

$("#form-login").submit((e) => {
    e.preventDefault();

    sendAjaxRequest('post', $('#form-login').serialize(), '/users/login', (res) => {
        if (res.status.toString().charAt(0) == "2") {
            // window.location.href = "/";
            showLoginPopout(`${res.message} Reloading the app...`, true, true);
            setTimeout(() => { window.location.reload(); }, 1500);
        } else {
            showLoginPopout(res.message, true);
        }
    });
});

$("#form-register").submit((e) => {
    e.preventDefault();

    sendAjaxRequest('post', $('#form-register').serialize(), '/users/register', (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showLoginPopout(res.message, true, true);
        } else {
            showRegisterPopout(res.message, true);
        }
    });
});

$("#form-forgot").submit((e) => {
    e.preventDefault();

    sendAjaxRequest('post', $('#form-forgot').serialize(), '/users/forgot', (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showForgotPopout(res.message, true, true);
        } else {
            showForgotPopout(res.message, true);
        }
    });

});

function initiateVerifyAccount(element) {
    $(element).text("Sent");
    $(element).prop("disabled", true);
    sendAjaxRequest('get', {}, `/users/verify`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showAlertMessage('success', res.message);
        } else {
            showAlertMessage('error', res.message);
        }
    });
}

function sendVerification(code) {
    sendAjaxRequest('get', {}, `/users/verify/${code}`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showAlertMessage('success', res.message);
        } else {
            showAlertMessage('error', res.message);
        }
    });
}

function showResetPopout() {
    const response = sendAjaxRequest('get', {}, `/users/forgot`, (res) => { });
    showMiniPopout({ id: "resetpassword", response: response });
}

function sendResetPassword(element) {
    sendAjaxRequest('patch', {
        password: $(element).parent().find('input[name="password"]').val(),
        newpassword: $(element).parent().find('input[name="newpassword"]').val(),
        confirmpassword: $(element).parent().find('input[name="confpassword"]').val()
    }, `/users/forgot/${getParameterByName('reset')}`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showAlertMessage('success', res.message);
            hideMiniPopout('minipopout-resetpassword');
        } else {
            showAlertMessage('error', res.message);
        }
    });
}

function showDisplayPicPopout() {
    const response = sendAjaxRequest('get', {}, `/users/account/displaypic`, (res) => { });
    showMiniPopout({ id: "editdp", response: response });
}

function showDisplayNamePopout() {
    const response = sendAjaxRequest('get', {}, `/users/account/displayname`, (res) => { });
    showMiniPopout({ id: "editdn", response: response });
}

function sendUpdatedProfile(element) {
    const minipopoutId = $(element).parent().parent().parent().parent().attr('id');
    const formId = $(element).parent().attr('id');
    let formData = new FormData(document.querySelector(`#${formId}`));

    sendAjaxImageRequest('patch', formData, `/users/account`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showAlertMessage('success', res.message);
            hideMiniPopout(minipopoutId);
        } else {
            showAlertMessage('error', res.message);
        }
    });
}

function showPrivacyPolicy() {
    const response = sendAjaxRequest('get', {}, `/users/privacy?json=true`, (res) => { });
    showMiniPopout({ id: "privacy", response: response });
}

function showTermsOfService() {
    const response = sendAjaxRequest('get', {}, `/users/terms?json=true`, (res) => { });
    showMiniPopout({ id: "terms", response: response });
}

// editing items
function editBook(bookId) {
    let lookupId = bookId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/books/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            newbookEndpoint = `/api/books/${lookupId}/edit`;
            newbookMethod = 'patch';
            showNewBookPopout();
            showCustomBookForm(null, res.book);
        } else {

        }
    });
}

function editClub(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/clubs/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            newclubEndpoint = `/api/clubs/${lookupId}/edit`;
            newclubMethod = 'patch';
            showNewClubPopout(res.club);
        } else {

        }
    });
}

function editComment(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/comments/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            commentEditPath = `/${res.comment.id}/edit`;
            commentEditMethod = 'patch';
            showCommentInput("Post a comment", "Comment");
            expandCommentInput(lookupId, "", res.comment);
        } else {

        }
    });
}
function editNote(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/notes/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            commentEditPath = `/${res.note.id}/edit`;
            commentEditMethod = 'patch';
            showCommentInput("Post a note", "Note");
            expandCommentInput(lookupId, "", res.note);
        } else {

        }
    });
}
function editMeeting(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/meetings/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            commentEditPath = `/${res.meeting.id}/edit`;
            commentEditMethod = 'patch';
            showCommentInput("Post a meeting", "Meeting");
            expandCommentInput(lookupId, "", res.meeting);
        } else {

        }
    });
}
function editPoll(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('get', {}, `/api/polls/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            commentEditPath = `/${res.poll.id}/edit`;
            commentEditMethod = 'patch';
            showCommentInput("Post a poll", "Poll");
            expandCommentInput(lookupId, "", res.poll);
        } else {

        }
    });
}
// deleting items
function deleteAlert(element) {
    const alertId = $(element).parent().parent().data('id');
    $(element).parent().parent().remove();
    sendAjaxRequest('delete', {}, `/api/alerts/${alertId}`, (res) => {
        if (res.status.toString().charAt(0) == "2") {

        } else {

        }
    });
}
function deleteBook(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/books/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            window.location.href = `/Books/`;
        } else {

        }
    });
}

function deleteClub(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/clubs/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            window.location.href = `/Clubs/`;
        } else {

        }
    });
}

function deleteComment(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/comments/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id);
        } else {

        }
    });
}
function deleteNote(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/notes/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id, `Notes`);
        } else {

        }
    });
}
function deleteMeeting(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/meetings/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id, `Meetings`);
        } else {

        }
    });
}
function deletePoll(resId) {
    let lookupId = resId.split("-")[0];
    sendAjaxRequest('delete', {}, `/api/polls/${lookupId}/edit`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id, `Polls`);
        } else {

        }
    });
}
// change settings
function changeSetting(event) {
    const element = event.target;
    let settingObj = {};
    if ($(element).parent().hasClass('setting-toggle')) {
        settingObj = { key: $(element).data('key'), value: $(element).is(':checked'), type: $(element).data('type') };
    }
    sendAjaxRequest('patch', { setting: settingObj }, `/api/settings/`, (res) => {
        if (res.status.toString().charAt(0) == "2") {

        } else {

        }
    });
}
//


function joinClub(clubId, element, joinCode) {
    $(element).parent().find(".error").addClass("d-none");

    sendAjaxRequest('post', { joinCode: joinCode }, `/api/clubs/${clubId}/join`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            // update join indicator
            if (res.evtcode == "joined") {
                element.classList.add("btn-secondary");
                element.classList.remove("btn-primary");
                element.innerText = "Joined";
            } else if (res.evtcode == "left") {
                element.classList.remove("btn-secondary");
                element.classList.add("btn-primary");
                element.innerText = "Join Club";
            }
            if (!joinCode) {
                setTimeout(() => { window.location.reload(); }, 1500);
            } else {
                window.location.href = `/Clubs/${clubId}`;
            }
        } else {
            showAlertMessage('error', res.message);
            // $(element).parent().parent().find(".error").text(res.message);
            // $(element).parent().parent().find(".error").removeClass("d-none");
        }
    });
}

function likeBook(bookId, element) {
    sendAjaxRequest('post', {}, `/api/books/${bookId}/like`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            if (res.evtcode == "liked") {
                $(element).find("use").attr("xlink:href", "#thumbsup-fill");
                $(element).find(".likecounter").text(res.newlikes);
            } else if (res.evtcode == "unliked") {
                $(element).find("use").attr("xlink:href", "#thumbsup");
                $(element).find(".likecounter").text(res.newlikes);
            }
        } else {

        }
    });
}

function bookmarkBook(elementId, element) {
    const bookId = $(`#${elementId}`).data("id");

    sendAjaxRequest('post', {}, `/api/books/${bookId}/bookmark`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            // update bookmarked indicator
            if (res.evtcode == "bookmarked") {
                $(element).find("use").attr("xlink:href", "#bookmark-fill");
            } else if (res.evtcode == "unbookmarked") {
                $(element).find("use").attr("xlink:href", "#bookmark");
            }
        } else {

        }
    });
}

function likeComment(commentId, element) {
    sendAjaxRequest('post', {}, `/api/comments/${commentId}/like`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            if (res.evtcode == "liked") {
                $(element).find("use").attr("xlink:href", "#thumbsup-fill");
                $(element).find(".likecounter").text(res.newlikes);
            } else if (res.evtcode == "unliked") {
                $(element).find("use").attr("xlink:href", "#thumbsup");
                $(element).find(".likecounter").text(res.newlikes);
            }
        } else {

        }
    });
}

function voteOnPoll(pollId, element) {
    $(element).parent().find('.error').addClass("d-none");

    let results = [];
    $(element).parent().find('.polloption').map(function (index, element) {
        let input = $(element).find('input');
        if (input.is(':checked')) {
            results.push(input.val());
        }
    });

    if (results.length > 0) {
        sendAjaxRequest('post', { votes: results }, `/api/polls/${pollId}/vote`, (res) => {
            if (res.status.toString().charAt(0) == "2") {
                // update voted indicator
                if (res.evtcode == "voted") {
                    showBook(getTopBookPopout().dataset.id, "", getTopBookPopout().id, `Polls`);
                }
            } else {
                $(element).parent().find('.error').text(res.message);
                $(element).parent().find('.error').removeClass("d-none");
            }
        });
    } else {
        $(element).parent().find('.error').text("Please make a selection!");
        $(element).parent().find('.error').removeClass("d-none");
    }
}

//alert badges
function updateAlertBadge(alertObj) {
    $("#alert-badge").text(alertObj.length);
    if (alertObj.length > 0) {
        $("#alert-badge").removeClass('d-none');
    } else {
        $("#alert-badge").addClass('d-none');
    }
}

function openAlert(element, e) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        if (!$(element).data('read')) {
            sendAjaxRequest('post', {}, `/api/alerts/${$(element).data('id')}/acknowledge`, (res) => {
                if (res.status.toString().charAt(0) == "2") {
                    $(element).removeClass('bg-unread');
                    $(element).addClass('bg-white');
                    updateAlertBadge(res.unreadAlerts);
                } else {

                }
            });
        }
        switch ($(element).data('code')) {
            case "comment_new":
                showBook($(element).data('refbook'), { target: "" }, null, null, $(element).data('refcomment'));
                break;
            case "comment_reply":
                showBook($(element).data('refbook'), { target: "" }, null, null, $(element).data('refcomment'));
                break;
            case "meeting_new":
                showBook($(element).data('refbook'), { target: "" }, null, "Meetings", $(element).data('refmeeting'));
                break;
            case "poll_new":
                showBook($(element).data('refbook'), { target: "" }, null, "Polls", $(element).data('refpoll'));
                break;
            case "book_new":
                showBook($(element).data('refbook'), { target: "" });
                break;
            case "club_joined":
                showClub($(element).data('refclub'), { target: "" });
                break;
        }
    }
}
//

function loadMoreSearch() {
    page += maxResults;
    buildBookResult(page, maxResults);
    if (page >= searchResults.length - maxResults) {
        $("#loadmore-link a").hide();
    }
}

function showCustomBookForm(chosenBook, editBook) {
    if (!editBook) {
        // $("#post-newbook").prop("disabled", false);
        $('#postbook-pane').find('.card-header').text('Post New Book');
        $('#post-newbook').text('Post');

        if (chosenBook) {
            if (chosenBook.olId != 'undefined') {
                $("#newbookolId").val(chosenBook.olId);
            } else {
                $("#newbookolId").val("");
                $("#newbookolId").attr("placeholder", "Open Library data or cover not available")
            }
            if (chosenBook.aId != 'undefined') {
                $("#newbookaId").val(chosenBook.aId);
            } else {
                $("#newbookaId").val("");
                $("#newbookaId").attr("placeholder", "Search-inside quote lookup not available")
            }

            $(".system-field").show();
            $("#newbookTitle").val(chosenBook.title);
            $("#newbookAuthor").val(chosenBook.author);
            $("#newbookPublished").val(chosenBook.published);
            $("#newbookImg").attr("src", chosenBook.coverImg);
            $("#newbookTags").val("");
            $("#newbookdescription .ql-editor").html("");
        } else {
            $(".system-field").hide();
            $("#newbookolId").val("");
            $("#newbookaId").val("");

            $("#newbookTitle").val("");
            $("#newbookAuthor").val("");
            $("#newbookPublished").val("");
            $("#newbookImg").attr("src", "/img/book-placeholder.jpg");
            $("#newbookTags").val("");
            $("#newbookdescription .ql-editor").html("");
        }
    } else {
        $("#post-newbook").prop("disabled", false);
        $('#postbook-pane').find('.card-header').text('Edit Your Book');
        $('#post-newbook').text('Save');

        if (editBook.olId) {
            $("#newbookolId").val(editBook.olId);
        } else {
            $("#newbookolId").val("");
            $("#newbookolId").attr("placeholder", "Open Library data or cover not available")
        }
        if (editBook.aId) {
            $("#newbookaId").val(editBook.aId);
        } else {
            $("#newbookaId").val("");
            $("#newbookaId").attr("placeholder", "Search-inside quote lookup not available")
        }

        $(".system-field").show();
        $("#newbookTitle").val(editBook.title);
        $("#newbookAuthor").val(editBook.author);
        $("#newbookPublished").val(editBook.publishedDate);
        $("#newbookImg").attr("src", editBook.coverThumb.url);
        $("#newbookTags").val(editBook.tags);
        $("#newbookdescription .ql-editor").html(editBook.description);
    }
    $("#resultbook-pane").hide();
    $("#postbook-pane").show();
}

function exitCustomBookForm() {
    $("#postbook-pane").hide();
    $("#resultbook-pane").show();
    clearFileInputs();
}

function openArchivePage(iaId) {
    window.open(`https://archive.org/details/${iaId}`, '_blank')
}

function searchInside(searchText, bookId) {

}

function selectResultBook(elem) {
    const parentid = $(elem).data("target");
    // const title = $(`#${parentid} .result-title`).text();
    // const author = $(`#${parentid} .result-author`).text();
    // const published = $(`#${parentid} .result-published`).text();
    const olId = $(`#${parentid}`).data("olId");
    const aId = $(`#${parentid}`).data("aId");
    const title = $(`#${parentid}`).data("title");
    const published = $(`#${parentid}`).data("published");
    const author = $(`#${parentid}`).data("author");
    const coverImg = $(`#${parentid} img`).attr("src");

    showCustomBookForm({
        olId,
        aId,
        title,
        published,
        author,
        coverImg
    })
    // editElem.dataset.olId = book.cover_edition_key;
    //     editElem.dataset.aId = book.aId;
    //     editElem.dataset.title = title;
    //     editElem.dataset.published = published;
    //     editElem.dataset.author = authors;
}
// new club popout
function showNewClubPopout(editData) {
    if (editData) {
        $('#postclub-pane').find('.card-header').text('Edit Your Club');
        $('#postclub-pane').find('button[type="submit"]').text('Save');

        $('#newclubIconInput').prop('required', false);
        $('#newclubName').val(editData.name);
        $('#newclubColor').val(editData.color);
        $('#newclubdescription .ql-editor').html(editData.description);
        $('#newclubImg').attr("src", editData.icon);
        $('#private-check').prop("checked", editData.private);
    } else {
        $('#postclub-pane').find('.card-header').text('Start a New Club');
        $('#postclub-pane').find('button[type="submit"]').text('Create');
        $('#newclubIconInput').prop('required', true);
    }
    $("#popout-newclub").animate({ right: 0 }, 300, () => {

    });
}
function hideNewClubPopout() {
    if (newbookEndpoint != '/api/clubs') {
        showAlertMessage('error', "Edit discarded");
    }
    newclubEndpoint = '/api/clubs';
    newclubMethod = 'post';

    clearFileInputs();

    $("#popout-newclub").animate({ right: "-100vw" }, 300, () => {

    });
}
////////////////////
function showPopout(contentObj, popoutType, refreshTargetId, gotoTab, highlightId) {
    // let $div = $("<div>", { id: "foo", "class": "text-muted" });
    // $div.html(contentObj.bookId);
    // $(".content-popout .container").append($div);
    let newPopout = $(`#${refreshTargetId}`);
    let editPopout = document.querySelector(`#${refreshTargetId}`);

    let tempId = (newPopout[0]) ? newPopout[0].id : "";
    let tempBookId = (newPopout[0] && newPopout[0].data) ? newPopout[0].data("tempBookId") : contentObj.id;

    if (!refreshTargetId) {
        newPopout = $('#popout-_template').clone();

        tempId = newPopout[0].id.replace("_template", contentObj.id);
        tempBookId = contentObj.id;
        if (document.querySelectorAll(`#${tempId}`).length > 0) {
            let i = 0;
            while (document.querySelectorAll(`#${tempId}-${i}`).length > 0) {
                i++;
            }
            tempId = `${tempId}-${i}`;
            tempBookId = `${contentObj.id}-${i}`
        }
        newPopout[0].id = tempId;
        newPopout.insertBefore($('#popout-_template'));
        // newPopout[0].innerHtml = newPopout[0].innerHtml.replace("_template", contentObj.id);
        editPopout = document.querySelector(`#${newPopout[0].id}`);
        editPopout.innerHTML = editPopout.innerHTML.replaceAll("_template", tempBookId);
        editPopout.classList.add(`Popout-${popoutType}`);
        editPopout.dataset.id = contentObj.id;
        editPopout.dataset.tempBookId = tempBookId;
        editPopout.dataset.popoutType = popoutType;

    }

    contentObj.response.then((res) => {
        editPopout.querySelector(".content-popout .container").innerHTML = res.view;
        editPopout.innerHTML = editPopout.innerHTML.replaceAll("_template", tempBookId);

        let topcolor = "#007bff";
        if (res.topcolor) {
            topcolor = res.topcolor;
        }
        if (popoutType == "Books") {
            $(`#${editPopout.id} .bookmark-button`).removeClass('d-none');
            $(editPopout).find('.popout-edit-item').attr("onclick", `editBook('${contentObj.id}')`);
            $(editPopout).find('.popout-delete-item').attr("onclick", `deleteBook('${contentObj.id}')`);
        } else if (popoutType == "Clubs") {
            $(editPopout).find('.popout-edit-item').first().attr("onclick", `editClub('${contentObj.id}')`);
            $(editPopout).find('.popout-delete-item').first().attr("onclick", `deleteClub('${contentObj.id}')`);
        } else {
            $(editPopout).find('dropdown-ellipsis').remove();
        }

        if (res.bookmark == true) {
            $(`#${editPopout.id} .bookmark-xlink`).attr("xlink:href", "#bookmark-fill");
        }
        $(`#${editPopout.id} nav`).css({ backgroundColor: topcolor })

        const hist = `/${popoutType}/${contentObj.id}/${res.slug}`;
        window.history.replaceState(hist, hist, hist);
        bootstrapInit();

        var triggerTabList = [].slice.call(document.querySelectorAll('#myTab li'))
        triggerTabList.forEach(function (triggerEl) {
            var tabTrigger = new bootstrap.Tab(triggerEl)

            triggerEl.addEventListener('click', function (event) {
                event.preventDefault();
                tabTrigger.show();
                switch ($(this).find("button").text()) {
                    case "Comments":
                        showCommentInput("Post a comment", "Comment");
                        break;
                    case "Notes":
                        showCommentInput("Post a note", "Note");
                        break;
                    case "Meetings":
                        showCommentInput("Post a meeting", "Meeting");
                        break;
                    case "Polls":
                        showCommentInput("Post a poll", "Poll");
                        break;
                }
            })
        });
        if (gotoTab) {
            const tabButtons = $(editPopout).find("button");
            tabButtons.each(function (index) {
                if ($(this).text() == gotoTab) {
                    $(this).parent().trigger('click');
                    $(this).trigger('click');
                }
            });
        }
        if (highlightId) {
            $(editPopout).find(`[data-id="${highlightId}"]`).addClass("highlighted");
        }
    })

    // $('.content-popout').animate({ right: 0 }, 300, () => {
    newPopout.animate({ right: 0 }, 300, () => {
        // show comment popout after content animates
        if (!refreshTargetId) {
            if (popoutType == "Books") {
                if (!gotoTab) {
                    showCommentInput("Post a comment", "Comment");
                }
            } else {
                hideCommentInput();
            }
        }
    });
}
function hidePopout(elementId) {
    window.history.back();
    $(`#${elementId}`).animate({ right: "-100vw" }, 300, () => {
        const popoutType = $(`#${elementId}`).data("popoutType");
        $(`#${elementId}`).remove();
        // reset club selector if no popouts
        const currentPopouts = $(".content-popout");
        if (currentPopouts.length <= 3) {
            selectClubById(homeClubId);
            if (getCurrentScreen() == "Books") {
                window.history.pushState(popoutType, popoutType, `/${popoutType}`);
            }
        }
    });
    hideCommentInput();
}
function getTopBookPopout() {
    const currentPopouts = $(".Popout-Books");
    if (currentPopouts.length > 0) {
        return currentPopouts[currentPopouts.length - 1];
    }
}
function getTopClubPopout() {
    const currentPopouts = $(".Popout-Clubs");
    if (currentPopouts.length > 0) {
        return currentPopouts[currentPopouts.length - 1];
    }
}
// login popout
function showLoginPopout(message, clearInputs, isSuccess) {
    const popoutElem = "#minipopout-login";
    const flashElem = "#minipopout-login-flash";
    if (message) {
        $(flashElem).text(message);
        $(flashElem).removeClass('d-none');
    } else {
        $(flashElem).text("");
        $(flashElem).addClass('d-none');
    }
    if (isSuccess) {
        $(flashElem).removeClass('alert-danger');
        $(flashElem).addClass('alert-success');
    } else {
        $(flashElem).addClass('alert-danger');
        $(flashElem).removeClass('alert-success');
    }
    if (clearInputs) {
        $("#form-login input").val("");
    }
    hideRegisterPopout();
    hideForgotPopout();
    $(popoutElem).removeClass('d-none');
    $(popoutElem).show();
    $(popoutElem).animate({ opacity: 1 }, 200, () => {
        // show comment popout after content animates

    });
}
function hideLoginPopout() {
    $('#minipopout-login').animate({ opacity: 0 }, 200, () => {
        $('#minipopout-login').hide();
    });
}
// register popout
function showRegisterPopout(message, clearInputs, isSuccess) {
    const popoutElem = "#minipopout-register";
    const flashElem = "#minipopout-register-flash";
    if (message) {
        $(flashElem).text(message);
        $(flashElem).removeClass('d-none');
    } else {
        $(flashElem).text("");
        $(flashElem).addClass('d-none');
    }
    if (isSuccess) {
        $(flashElem).removeClass('alert-danger');
        $(flashElem).addClass('alert-success');
    } else {
        $(flashElem).addClass('alert-danger');
        $(flashElem).removeClass('alert-success');
    }
    if (clearInputs) {
        $("#newconfpassword-input").val("");
    }
    hideLoginPopout();
    hideForgotPopout();
    $(popoutElem).removeClass('d-none');
    $(popoutElem).show();
    $(popoutElem).animate({ opacity: 1 }, 200, () => {
        // show comment popout after content animates

    });
}
function hideRegisterPopout() {
    $('#minipopout-register').animate({ opacity: 0 }, 200, () => {
        $('#minipopout-register').hide();
    });
}
// forgot popout
function showForgotPopout(message, clearInputs, isSuccess) {
    const popoutElem = "#minipopout-forgot";
    const flashElem = "#minipopout-forgot-flash";
    if (message) {
        $(flashElem).text(message);
        $(flashElem).removeClass('d-none');
    } else {
        $(flashElem).text("");
        $(flashElem).addClass('d-none');
    }
    if (isSuccess) {
        $(flashElem).removeClass('alert-danger');
        $(flashElem).addClass('alert-success');
    } else {
        $(flashElem).addClass('alert-danger');
        $(flashElem).removeClass('alert-success');
    }
    if (clearInputs) {
    }
    hideLoginPopout();
    hideRegisterPopout();
    $(popoutElem).removeClass('d-none');
    $(popoutElem).show();
    $(popoutElem).animate({ opacity: 1 }, 200, () => {
        // show comment popout after content animates

    });
}
function hideForgotPopout() {
    $('#minipopout-forgot').animate({ opacity: 0 }, 200, () => {
        $('#minipopout-forgot').hide();
    });
}
// mini popout
function showMiniPopout(contentObj) {
    // let $div = $("<div>", { id: "foo", "class": "text-muted" });
    // $div.html(contentObj.bookId);
    // $(".content-popout .container").append($div);
    const newPopout = $('#minipopout-_template').clone();
    newPopout[0].id = newPopout[0].id.replace("_template", contentObj.id);
    newPopout.insertBefore($('#minipopout-_template'));
    // newPopout[0].innerHtml = newPopout[0].innerHtml.replace("_template", contentObj.id);
    let editPopout = document.querySelector(`#${newPopout[0].id}`);
    editPopout.classList.remove("d-none");

    contentObj.response.then((res) => {
        editPopout.querySelector(".card-minipopout .card-body").innerHTML = res.view;
        editPopout.innerHTML = editPopout.innerHTML.replaceAll("_template", contentObj.id);
        $(editPopout).find(".img-upload").find("input").on("change", changeImgPreview)
    });
    // contentObj.response.then((res) => {
    //     editPopout.querySelector(".content-popout .container").innerHTML = res.data.view;
    //     const hist = `/Books/${contentObj.id}/${res.data.slug}`;
    //     window.history.pushState(hist, hist, hist);

    // })

    // $('.content-popout').animate({ right: 0 }, 300, () => {
    newPopout.animate({ opacity: 1 }, 200, () => {
        // show comment popout after content animates

    });
}
function hideMiniPopout(elementId) {
    $(`#${elementId}`).animate({ opacity: 0 }, 200, () => {
        $(`#${elementId}`).remove();
    });
}
//
let filter = {};
function selectFilter(filterObj, element) {
    filter = filterObj;
    const topPopout = getTopClubPopout();
    moveScreen($(`#nav-Books`).attr("data-page"), $(`#nav-Books`)[0], true);
    if (topPopout) {
        showClub(topPopout.dataset.id, "", topPopout.id);
    }
    element.classList.remove("btn-light");
    element.classList.add("btn-secondary");
}

function getCurrentScreen() {
    return $("#nav-sidebar").find('.active').data('pagename');
}

function moveScreen(page, element, instant) {
    $('.nav-link').removeClass("active");

    element.classList.add("active");
    const { pagename, onload } = element.dataset;

    let queryGet = "";
    if (selectedClubId && selectedClubId !== "allclubs") {
        queryGet = `?c=${selectedClubId}`;
    }

    sendAjaxRequest('get', filter, `/api/${onload}${queryGet}`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            $(`#p-${pagename}`).html(res.view);
            if (onload == "Alerts") {
                updateAlertBadge(res.unreadAlerts);
            }
            if (onload == "Settings") {
                $('.setting-toggle input').on('change', changeSetting);
            }
            if (onload == "Meetings") {
                let calendarEl = document.getElementById('calendar');
                let calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                    events: res.meetings
                    // events: [
                    //     {
                    //         "id": 293,
                    //         "title": "Event 1",
                    //         "url": "http://example.com",
                    //         "start": new Date(), // Milliseconds
                    //         "end": new Date() + 3600000, // Milliseconds
                    //         daysOfWeek: ['4'],
                    //         startRecur: new Date(),
                    //         color: "#F0F0F0",
                    //     }]
                });
                calendar.render();
            }
        } else {

        }
    });


    let position = `-${page * 100}vw`;
    if (!instant) {
        window.history.pushState(pagename, pagename, `/${pagename}`);
        $('#content-main').animate({ left: position }, 300, () => {
            // do after
        });
    } else {
        $('#content-main').css({ left: position });
    }
}

const sideNavLinks = document.querySelectorAll("#nav-sidebar ul li a");
sideNavLinks.forEach((element, index) => {
    element.addEventListener('click', (e) => {
        moveScreen(index, element);
    });
});

//
$("#post-newbook").prop("disabled", true);
let selectedClubId = "allclubs";
let homeClubId = "allclubs";
function selectClub(elem, autoSelect) {

    const parentid = $(elem).data("parentid");
    const clubid = $(elem).data("clubid")
    const newicon = $(elem).data("clubicon")
    const newname = $(elem).data("clubname")
    const newcolor = $(elem).data("clubcolor")

    $(".club-selector img").attr("src", newicon);
    $(".club-selector img").css("border-color", newcolor);
    $(".club-selector span").text(newname);

    if (clubid != "allclubs" && clubid) {
        selectedClubId = clubid;
        if (!autoSelect) {
            // homeClubId = clubid;
        }
        $("#post-newbook").prop("disabled", false);
    } else {
        selectedClubId = "";
        if (!autoSelect) {
            // homeClubId = "allclubs";
        }
        $("#post-newbook").prop("disabled", true);
        $("#newbook-club-dropdown img").attr("src", "/img/bookstack.png");
        $("#newbook-club-dropdown img").css("border-color", "#ebebeb");
        $("#newbook-club-dropdown span").text("Select a club to post to...");
    }
    const currentPopouts = $(".content-popout");
    if (currentPopouts.length <= 3 && getCurrentScreen() == "Books") {
        moveScreen($(`#nav-Books`).attr("data-page"), $(`#nav-Books`)[0], true);
    }
}

function selectClubById(clubId) {
    const clubSelectorElem = $("div.dropdown-item[data-clubid='" + clubId + "']");
    if (clubSelectorElem.length > 0) {
        selectClub(clubSelectorElem[0], true);
    }
}

//// startup
function onLoad() {
    let navTo = window.location.pathname.substr(1).split("/")[0];
    let resId = window.location.pathname.substr(1).split("/")[1];
    let actionTo = window.location.pathname.substr(1).split("/")[2];
    let dontContinue = false;

    if (!navTo || navTo == '') {
        navTo = "Books";
    };
    moveScreen($(`#nav-${navTo}`).attr("data-page"), $(`#nav-${navTo}`)[0], true);

    if (actionTo) {
        switch (actionTo) {
            case "join": showJoinPopout(resId, getParameterByName('joinCode')); dontContinue = true;
                break;
        }
    }
    if (getParameterByName('verify')) {
        sendVerification(getParameterByName('verify'));
    }
    if (getParameterByName('reset')) {
        showResetPopout();
    }

    if (resId && !dontContinue) {
        switch (navTo) {
            case "Books": showBook(resId, { target: "" });
                break;
            case "Clubs": showClub(resId, { target: "" });
                break;
        }
    }
}
onLoad();

function showBook(bookId, e, refreshTargetId, gotoTab, highlightId) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        const hist = `/Books/${bookId}`;
        window.history.pushState(hist, hist, hist);

        const response = sendAjaxRequest('get', filter, `/api/Books/${bookId}`, (res) => { });

        showPopout({ id: bookId, response: response }, "Books", refreshTargetId, gotoTab, highlightId);
    }
}

function showClub(clubId, e, refreshTargetId) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        // const clubSelectorElem = $("div.dropdown-item").find(`[data-clubid='${clubId}']`)
        selectClubById(clubId);

        const hist = `/Clubs/${clubId}`;
        window.history.pushState(hist, hist, hist);

        const response = sendAjaxRequest('get', filter, `/api/Clubs/${clubId}`, (res) => { });

        showPopout({ id: clubId, response: response }, "Clubs", refreshTargetId);
    }
}

function showSharePopout(clubId, e) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        const response = sendAjaxRequest('get', {}, `/api/Clubs/${clubId}/invite`, (res) => { });

        showMiniPopout({ id: clubId, response: response }, "Clubs");
    }
}

function showMembersPopout(clubId, e) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        const response = sendAjaxRequest('get', {}, `/api/Clubs/${clubId}/members`, (res) => {
            if (res.status.toString().charAt(0) == "2") {

            } else {
                showAlertMessage('error', res.message);
            }
        });
        showMiniPopout({ id: clubId, response: response });
    }
}

function updateClubMember(clubId, memberId, action, e) {
    sendAjaxRequest('patch', { memberId: memberId, action: action }, `/api/Clubs/${clubId}/members`, (res) => {
        if (res.status.toString().charAt(0) == "2") {
            showAlertMessage('success', res.message);
            switch (res.evtcode) {
                case "kicked":
                    $(e).parent().parent().remove();
                    break;
                case "promoted":
                    $(e).find("use").attr("xlink:href", "#is-owner");
                    break;
                case "demoted":
                    $(e).find("use").attr("xlink:href", "#isnot-owner");
                    break;
            }
            if (res.evtcode == "demoted") {
            }

        } else {
            showAlertMessage('error', res.message);
        }
    });
}


function showJoinPopout(clubId, joinCode, e) {
    if (!e || (!$(e.target).is('a') && !$(e.target).is('button') && !$(e.target).is('svg') && !$(e.target).is('span') && !$(e.target).is('li'))) {
        const response = sendAjaxRequest('get', {}, `/api/Clubs/${clubId}/join?joinCode=${joinCode}`, (res) => { });

        showMiniPopout({ id: clubId, joinCode: joinCode, response: response }, "Clubs");
    }
}

function copyInput(element) {
    copyToClipboard(`${location.protocol}//${url_domain()}/Clubs/${$(element).parent().find('input').data("clubid")}/join?joinCode=${$(element).parent().find('input').data('code')}`);
    // copyToClipboard(`${$(element).parent().find('input').data('code')}`);
    $(element).find("use").attr("xlink:href", "#clipboard-copied");
}

function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

function url_domain(data) {
    var a = document.createElement('a');
    a.href = data;
    return a.hostname;
}


async function sendAjaxRequest(method, dataObj, path, onSuccess) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${location.protocol}//${url_domain()}${path}`,
            type: method,
            dataType: 'json',
            data: dataObj,
            success: function (res) {
                if (res.status == 401) {
                    showLoginPopout("You must log in to do this!");
                } else {
                    onSuccess(res);
                }
                resolve(res);
            },
            error: function (res) {
                onSuccess(res);
                reject(res);
            }
        });
    });
}

async function sendAjaxImageRequest(method, dataObj, path, onSuccess) {
    // let formData = new FormData(this);

    $.ajax({
        type: method,
        url: `${location.protocol}//${url_domain()}${path}`,
        data: dataObj,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status == 401) {
                showLoginPopout("You must log in to do this!");
            } else {
                onSuccess(res);
            }
        },
        error: function (res) {
            onSuccess(res);
        }
    });
}

////////////

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function showAlertMessage(type, message, seconds = 3) {
    // const newAlert = $('body').append(`<div class='popdown-message ${type}'>${message}</div>`);
    const newAlertEl = $(`<div class='popdown-message ${type}'>${message}</div>`);
    $('body').append(newAlertEl);
    $(newAlertEl).animate({ top: 0 }, 300, () => { })

    setTimeout((element) => {
        $(element).animate({ top: -64 }, 300, () => {
            $(element).remove();
        })
    }, seconds * 1000, newAlertEl);

}

$(".img-upload").find("input").on("change", changeImgPreview)
function changeImgPreview(e) {
    const img = $(e.target).parent().parent().parent().find('img');
    const label = $(e.target).parent().parent().parent().find('label');
    const files = $(e.target).prop('files');
    img.attr("src", URL.createObjectURL(files[0]));
    label.text(files[0].name);
}

function clearFileInputs() {
    $(".img-upload").find("img").attr("src", "/img/book-placeholder.jpg");
    $(".img-upload").find("input").val(null);
    $(".img-upload").find("label").text("Choose file")
}