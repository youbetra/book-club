const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Poll } = require('../models/poll');
const polls = require('../controllers/polls');
// display pici  components


router.route('/')
    .post(isLoggedIn, catchAsync(polls.newPoll));

router.route('/:pollId/vote')
    .post(isLoggedIn, catchAsync(polls.voteOnPoll));

router.route('/:pollId/edit')
    .get(isLoggedIn, catchAsync(polls.getEditData))
    .patch(isLoggedIn, catchAsync(polls.editPoll))
    .delete(isLoggedIn, catchAsync(polls.deletePoll));

module.exports = router;