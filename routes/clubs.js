const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Club } = require('../models/club');
const clubs = require('../controllers/clubs');
// display pici  components
const multer = require('multer');
const { storage } = require('../cloudinary');
const upload = multer({ storage });


router.route('/')
    .post(isLoggedIn, upload.single('newclub-icon'), catchAsync(clubs.newClub))
    .get(catchAsync(clubs.getClubs));

router.route('/:clubId')
    .get(catchAsync(clubs.getClub));

router.route('/:clubId/join')
    .get(isLoggedIn, catchAsync(clubs.prejoinClub))
    .post(isLoggedIn, catchAsync(clubs.joinClub));

router.route('/:clubId/invite')
    .get(isLoggedIn, catchAsync(clubs.getClubInvitation));

router.route('/:clubId/edit')
    .get(isLoggedIn, catchAsync(clubs.getEditData))
    .patch(isLoggedIn, upload.single('newclub-icon'), catchAsync(clubs.editClub))
    .delete(isLoggedIn, catchAsync(clubs.deleteClub));

router.route('/:clubId/members')
    .get(isLoggedIn, catchAsync(clubs.getMemberList))
    .patch(isLoggedIn, catchAsync(clubs.updateMember))


module.exports = router;