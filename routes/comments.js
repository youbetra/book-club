const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Comment } = require('../models/comment');
const comments = require('../controllers/comments');
// display pici  components


router.route('/')
    .post(isLoggedIn, catchAsync(comments.newComment));

router.route('/:commentId/like')
    .post(isLoggedIn, catchAsync(comments.likeComment))

router.route('/:commentId/edit')
    .get(isLoggedIn, catchAsync(comments.getEditData))
    .patch(isLoggedIn, catchAsync(comments.editComment))
    .delete(isLoggedIn, catchAsync(comments.deleteComment));

module.exports = router;