const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Meeting } = require('../models/meeting');
const meetings = require('../controllers/meetings');
// display pici  components


router.route('/')
    .get(catchAsync(meetings.getMeetings))
    .post(isLoggedIn, catchAsync(meetings.newMeeting))

router.route('/:meetingId/edit')
    .get(isLoggedIn, catchAsync(meetings.getEditData))
    .patch(isLoggedIn, catchAsync(meetings.editMeeting))
    .delete(isLoggedIn, catchAsync(meetings.deleteMeeting));

module.exports = router;