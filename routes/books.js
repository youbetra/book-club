const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Book } = require('../models/book');
const books = require('../controllers/books');
// display pici  components
const multer = require('multer');
const { storage } = require('../cloudinary');
const upload = multer({ storage });


router.route('/')
    .post(isLoggedIn, upload.single('newbook-image'), catchAsync(books.newBook))
    .get(catchAsync(books.getBooks));

router.route('/:bookId')
    .get(catchAsync(books.getBook));

router.route('/:bookId/like')
    .post(isLoggedIn, catchAsync(books.likeBook))

router.route('/:bookId/bookmark')
    .post(isLoggedIn, catchAsync(books.bookmarkBook))


router.route('/:bookId/edit')
    .get(isLoggedIn, catchAsync(books.getEditData))
    .patch(isLoggedIn, upload.single('newbook-image'), catchAsync(books.editBook))
    .delete(isLoggedIn, catchAsync(books.deleteBook));

module.exports = router;