const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Alert } = require('../models/alert');
const alerts = require('../controllers/alerts');
// display pici  components


router.route('/')
    .get(isLoggedIn, catchAsync(alerts.getAlerts))

router.route('/:alertId')
    .delete(isLoggedIn, catchAsync(alerts.deleteAlert))

router.route('/:alertId/acknowledge')
    .post(isLoggedIn, catchAsync(alerts.acknowledgeAlert))

module.exports = router;