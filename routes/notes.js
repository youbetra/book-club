const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn } = require('../middleware');
const { Note } = require('../models/note');
const notes = require('../controllers/notes');
// display pici  components


router.route('/')
    .post(isLoggedIn, catchAsync(notes.newNote))
    .delete(isLoggedIn, catchAsync(notes.deleteNote));

router.route('/:noteId/edit')
    .get(isLoggedIn, catchAsync(notes.getEditData))
    .patch(isLoggedIn, catchAsync(notes.editNote))
    .delete(isLoggedIn, catchAsync(notes.deleteNote));

module.exports = router;