const express = require('express');
const router = express.Router();
const passport = require('passport');
const catchAsync = require('../utils/catchAsync');
const { isLoggedIn, usernameToLowerCase } = require('../middleware');
const { User } = require('../models/user');
const users = require('../controllers/users');
// display pici  components
const multer = require('multer');
const { storage } = require('../cloudinary');
const upload = multer({ storage });

router.route('/register')
    .post(usernameToLowerCase, catchAsync(users.register));

router.route('/account')
    .get(isLoggedIn, users.renderAccount)
    .patch(isLoggedIn, upload.single('displaypic'), catchAsync(users.updateAccount));

router.route('/account/displaypic')
    .get(isLoggedIn, users.getDPPopout)
    .patch(isLoggedIn, catchAsync(users.updateDisplayPic))

router.route('/account/displayname')
    .get(isLoggedIn, users.getDNPopout)
    .patch(isLoggedIn, catchAsync(users.updateDisplayName))


router.route('/verify')
    .get(isLoggedIn, catchAsync(users.resendVerification))

router.route('/verify/:verifToken')
    .get(isLoggedIn, catchAsync(users.verifyAccount))

router.route('/login')
    .get(users.renderLogin)
    .post(usernameToLowerCase, users.login)

router.route('/auth/google')
    .get(passport.authenticate('google', { scope: ['email', 'profile'] }));

router.route('/auth/google/callback')
    .get(passport.authenticate('google', { failureRedirect: '/users/login' }),
        function (req, res) {
            // Successful authentication, redirect home.
            res.redirect('/');
        });

router.route('/forgot')
    .get(users.getResetPopout)
    .post(catchAsync(users.initiateForgot))

router.route('/forgot/:resetToken')
    .patch(catchAsync(users.submitResetForm))

router.get('/logout', users.logout)

router.get('/privacy', users.showPrivacyPolicy)
router.get('/terms', users.showTermsOfService)

module.exports = router;