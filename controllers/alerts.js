const { Alert } = require('../models/alert');
const { User } = require('../models/user');
const stringToBoolean = require('../utils/stringToBoolean');

module.exports.getAlerts = async (req, res, next) => {
    try {
        if (req.user) {
            let user = await User.findById(req.user._id).populate({
                path: 'alerts',
                populate: 'refBook refMeeting refPoll refComment refParentComment refClub refUser',
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            });
            let unreadUser = await User.findById(req.user._id).populate({
                path: 'alerts',
                match: {
                    'deleted': {
                        $ne: true
                    },
                    'read': {
                        $ne: true
                    }
                }
            });

            res.renderWithData('layouts/alerts', { alerts: user.alerts }, { status: 200, unreadAlerts: unreadUser.alerts });
        } else {
            res.renderWithData('layouts/alerts', { alerts: [] }, { status: 200 });
        }
    } catch (e) {
        res.renderWithData('layouts-popout/error', {}, { status: 500, message: e.message });
    }
}

module.exports.acknowledgeAlert = async (req, res, next) => {
    try {
        const { alertId } = req.params;

        let alert = await Alert.findById(alertId);
        if (alert) {
            if (req.user._id.toString() == alert.user.toString()) {
                alert.read = true;
                await alert.save();

                let unreadUser = await User.findById(req.user._id).populate({
                    path: 'alerts',
                    match: {
                        'deleted': {
                            $ne: true
                        },
                        'read': {
                            $ne: true
                        }
                    }
                });

                res.json({ status: 200, unreadAlerts: unreadUser.alerts });
            } else {
                throw { status: 403, message: "The alert you're acknowledging on doesn't belong to you!" };
            }
        } else {
            throw { status: 404, message: "The alert you're acknowledging on doesn't appear to exist!" };
        }
    } catch (e) {
        res.renderWithData('layouts-popout/error', {}, { status: 500, message: e.message });
    }
}

module.exports.deleteAlert = async (req, res, next) => {
    try {
        const { alertId } = req.params;

        let alert = await Alert.findById(alertId);
        if (alert) {
            if (req.user._id.toString() == alert.user.toString()) {
                alert.deleted = true;
                await alert.save();

                res.json({ status: 200, message: "the alert was successfully deleted." });
            } else {
                throw { status: 403, message: "The alert you're deleting doesn't belong to you!" };
            }
        } else {
            throw { status: 404, message: "The alert you're deleting doesn't appear to exist!" };
        }
    } catch (e) {
        res.renderWithData('layouts-popout/error', {}, { status: 500, message: e.message });
    }
}