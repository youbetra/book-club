const { User } = require('../models/user');
const passport = require('passport');
const encodeImage = require('../utils/encodeImage');
const dateAdd = require('../utils/dateAdd');
const randomstring = require("randomstring");
const { sendEmail } = require('../utils/sendEmail');

async function sendVerificationEmail(userName, emailAddress, verificationToken) {
    await sendEmail({
        to: emailAddress,
        subject: "Email Verification for Book Club",
        bodyText: `Hello ${userName},
        \r\n\r\nThis email address has been added to your Book Club account. Before you can start posting books and creating clubs, please verify your email address.
        \r\n\r\nhttps://bookclub.tetrayoubetra.com?verify=${verificationToken}
        \r\n\r\nIf ${userName} is not your Book Club account, let us know and we'll make sure nothing weird is going on.
        \r\n\r\nBook Club
        \r\nhttps://bookclub.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>This email address has been added to your Book Club account. Before you can start posting books and creating clubs, please verify your email address.</p>
        <p style="text-align: center"><a href="https://bookclub.tetrayoubetra.com?verify=${verificationToken}">https://bookclub.tetrayoubetra.com?verify=${verificationToken}</a></p>
        <p>If ${userName} is not your Book Club account, let us know and we'll make sure nothing weird is going on.</p>
        <p style="text-align: center"><a href="https://bookclub.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Book Club Logo">
        <br>https://bookclub.tetrayoubetra.com/</a></p>`
    });
}

async function sendResetEmail(userName, emailAddress, resetToken) {
    await sendEmail({
        to: emailAddress,
        subject: "Password Reset for Book Club",
        bodyText: `Hello ${userName},
        \r\n\r\nA password reset has been requested for your account. The link below will take you to a password reset form.
        \r\n\r\nhttps://bookclub.tetrayoubetra.com?reset=${resetToken}
        \r\n\r\nIf ${userName} is not your Book Club account or you did not initiate this reset, delete this message and let us know if you feel anything fishy is occurring.
        \r\n\r\nBook Club?!
        \r\nhttps://bookclub.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>A password reset has been requested for your account. The link below will take you to a password reset form.</p>
        <p style="text-align: center"><a href="https://bookclub.tetrayoubetra.com?reset=${resetToken}">https://bookclub.tetrayoubetra.com?reset=${resetToken}</a></p>
        <p>If ${userName} is not your Book Club account or you did not initiate this reset, delete this message and let us know if you feel anything fishy is occurring.</p>
        <p style="text-align: center"><a href="https://bookclub.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Book Club Logo">
        <br>https://bookclub.tetrayoubetra.com/</a></p>`
    });
}

async function sendPasswordChangeEmail(userName, emailAddress) {
    await sendEmail({
        to: emailAddress,
        subject: "Password Changed on Book Club",
        bodyText: `Hello ${userName},
        \r\n\r\nYour password was just now successfully changed on Book Club.
        \r\n\r\nhttp://bookclub.tetrayoubetra.com/users/login#forgot
        \r\n\r\nIf you did not initiate this reset, please immediately re-aquire access to your account via the Forgot Password method.
        \r\n\r\nWe recommend changing your password to something more secure!
        \r\n\r\nBook Club?!
        \r\nhttps://bookclub.tetrayoubetra.com/`,
        bodyHtml: `Hello ${userName},
        <p>Your password was just now successfully changed on Book Club.</p>
        <p style="text-align: center"><a href="http://bookclub.tetrayoubetra.com/users/login#forgot">http://bookclub.tetrayoubetra.com/users/login#forgot</a></p>
        <p>If you did not initiate this reset, please immediately re-aquire access to your account via the Forgot Password method. We recommend changing your password to something more secure!</p>
        <p style="text-align: center"><a href="https://bookclub.tetrayoubetra.com/"><img src="data:image/png;base64,${encodeImage('./public/img/logo.png')}" alt="Book Club Logo">
        <br>https://bookclub.tetrayoubetra.com/</a></p>`
    });
}

module.exports.renderAccount = (req, res) => {
    res.render('users/account');
}

module.exports.updateAccount = async (req, res, next) => {
    try {
        const { displayname } = req.body;
        const user = await User.findById(req.user._id);
        if (displayname && displayname != req.user.displayname) {
            user.displayname = displayname;
        }

        if (req.file) {
            console.log("uploading display pic")
            user.image.url = req.file.path;
            user.image.filename = req.file.filename;
        }
        await user.save();

        res.json({ status: 200, message: 'Successfully updated account.' });
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.verifyAccount = async (req, res) => {
    const { verifToken } = req.params;
    if (req.user.emailVerified == true) {
        res.json({ status: 400, message: 'Your email is already verified!' });
    } else {
        if (req.user.verificationToken == verifToken) {
            await User.findByIdAndUpdate(req.user._id, { emailVerified: true, verificationToken: undefined });
            res.json({ status: 200, message: 'Successfully verified your email!' });
        } else {
            res.json({ status: 500, message: 'Failed to verify your email, please try again.' });
        }
    }
}

module.exports.resendVerification = async (req, res) => {
    const user = await User.findByIdAndUpdate(req.user._id, { verificationToken: randomstring.generate() }, { new: true });
    sendVerificationEmail(user.username, user.email, user.verificationToken);

    res.json({ status: 200, message: "Resent verification email." });
}

module.exports.register = async (req, res, next) => {
    try {
        const { displayname, username, password } = req.body;
        const user = new User({ displayname, email: username, username, image: { url: "/img/person-placeholder.jpg", filename: "/img/person-placeholder.jpg" }, verificationToken: randomstring.generate() });
        const registeredUser = await User.register(user, password);
        req.login(registeredUser, err => {
            if (err) return next(err);
            sendVerificationEmail(registeredUser.username, registeredUser.email, registeredUser.verificationToken);
            req.flash('success', 'Welcome!');
            res.json({ status: 200, message: "Successfully registered" });
        })
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.renderLogin = (req, res) => {
    res.json({});
}

module.exports.login = (req, res, next) => {
    passport.authenticate('local', function (err, user, info) {
        if (err) { return res.json({ status: 400, message: err }); }
        if (!user) { return res.json({ status: 400, message: "Incorrect username or password!" }); }
        req.logIn(user, function (err) {
            if (err) { return res.json({ status: 400, message: err }); }
            return res.json({ status: 200, message: 'Successfully logged in.', user: user.displayname });
        });
    })(req, res, next);
}

module.exports.logout = (req, res) => {
    req.logout();
    // req.session.destroy();
    // req.flash('success', "Goodbye!");
    res.redirect("/");
}

module.exports.initiateForgot = async (req, res) => {
    const { email } = req.body;
    const user = await User.findOneAndUpdate({ email: email }, { resetToken: randomstring.generate(), resetTokenExp: dateAdd(new Date(), 'minute', 30) }, { new: true });
    if (user) {
        sendResetEmail(user.username, user.email, user.resetToken);
        console.log("found user and reset");
    } else {
        console.log("failed to find user and reset");
    }
    res.json({ status: 200, message: "If the email was found, you will receive a password reset email." });
}

module.exports.getResetPopout = (req, res) => {
    res.renderWithData('layouts-minipopout/reset', {}, { status: 200 });
}

module.exports.getDPPopout = (req, res) => {
    res.renderWithData('layouts-minipopout/displaypic', {}, { status: 200 });
}

module.exports.updateDisplayPic = (req, res) => {
}

module.exports.getDNPopout = (req, res) => {
    res.renderWithData('layouts-minipopout/displayname', {}, { status: 200 });
}

module.exports.updateDisplayName = (req, res) => {
}

module.exports.renderResetForm = (req, res) => {
    const { resetToken } = req.params;
    res.render('users/reset', { resetToken });
}

module.exports.submitResetForm = async (req, res) => {
    const { resetToken } = req.params;
    const { password, newpassword, confirmpassword } = req.body;
    let user;
    if (!req.user) {
        // reset token reset
        user = await User.findOne({ resetToken: resetToken });
        if (user && user.resetTokenExp > new Date()) {
            if (newpassword && newpassword == confirmpassword) {
                console.log(`changing password`)
                let passUser = await User.findById(user._id);
                try {
                    await passUser.setPassword(newpassword);
                    console.log("successfully changed password")
                    res.json({ status: 200, message: "Successfully changed your password." });
                    sendPasswordChangeEmail(passUser.username, passUser.email);
                } catch (e) {
                    console.log(e);
                    res.json({ status: 500, message: e.message });
                }
                passUser.resetToken = undefined;
                passUser.resetTokenExp = undefined;
                await passUser.save();
            } else {
                res.json({ status: 500, message: 'The passwords entered did not match.' });
            }

        } else {
            res.json({ status: 500, message: 'The reset token has expired, please try again.' });
        }
    } else {
        // logged in user reset
        user = await User.findOne({ _id: req.user._id });
        if (user && password && newpassword == confirmpassword) {
            console.log(`changing password`)
            let passUser = await User.findById(req.user._id);
            try {
                await passUser.changePassword(password, newpassword);
                console.log("successfully changed password")
                res.json({ status: 200, message: 'Successfully changed your password.' });

                sendPasswordChangeEmail(passUser.username, passUser.email);
            } catch (e) {
                console.log(e);
                res.json({ status: 500, message: e.message });
            }
            await passUser.save();
        } else {
            res.json({ status: 400, message: 'The passwords entered did not match.' });
        }
    }
    console.log(`setting password for ${user.username} to: ${newpassword} - ${confirmpassword}`)



}

module.exports.showPrivacyPolicy = async (req, res) => {
    const { json = false } = req.query;
    if (json) {
        res.renderWithData('layouts-minipopout/privacypolicy', {}, { status: 200 });
    } else {
        res.render('layouts-minipopout/privacypolicy', {});
    }
}

module.exports.showTermsOfService = async (req, res) => {
    const { json = false } = req.query;
    if (json) {
        res.renderWithData('layouts-minipopout/termsofservice', {}, { status: 200 });
    } else {
        res.render('layouts-minipopout/termsofservice', {});
    }
}