const { User } = require('../models/user');
const stringToBoolean = require('../utils/stringToBoolean');

module.exports.getSettings = async (req, res, next) => {
    try {
        if (req.user) {
            let user = await User.findById(req.user._id);
            if (user) {

                let settings = user.settings;
                const settingsList = [{
                    heading: "Theme",
                    options: [{
                        label: "Dark Mode",
                        value: settings.darkmode,
                        type: "toggle",
                        key: "darkmode"
                    }]
                }, {
                    heading: "Performance",
                    options: [{
                        label: "Reduce Animations",
                        value: settings.reduceanims,
                        type: "toggle",
                        key: "reduceanims"
                    }, {
                        label: "Reduce Effects",
                        value: settings.reduceeffects,
                        type: "toggle",
                        key: "reduceeffects"
                    }]
                }, {
                    heading: "Alerts",
                    options: [
                        // {
                        //     label: "Send Alert Emails",
                        //     value: settings.emailalerts,
                        //     type: "toggle",
                        //     key: "emailalerts"
                        // }, 
                        {
                            label: "Public Club Alerts",
                            value: settings.alertpublic,
                            type: "toggle",
                            key: "alertpublic"
                        }, {
                            label: "Alert on New Comments",
                            value: settings.alerts.comments,
                            type: "toggle",
                            key: "alerts.comments"
                        }, {
                            label: "Alert on New Replies",
                            value: settings.alerts.replies,
                            type: "toggle",
                            key: "alerts.replies"
                        }, {
                            label: "Alert on New Meetings",
                            value: settings.alerts.meetings,
                            type: "toggle",
                            key: "alerts.meetings"
                        }, {
                            label: "Alert on New Polls",
                            value: settings.alerts.polls,
                            type: "toggle",
                            key: "alerts.polls"
                        }, {
                            label: "Alert on New Books",
                            value: settings.alerts.books,
                            type: "toggle",
                            key: "alerts.books"
                        }, {
                            label: "Alert on New Club Members",
                            value: settings.alerts.newmembers,
                            type: "toggle",
                            key: "alerts.newmembers"
                        }]
                }]
                res.renderWithData('layouts/settings', { settingsList }, { status: 200 });
            } else {
                throw { status: 500, message: "An error occurred retrieving your settings." };
            }
        } else {
            throw { status: 403, message: "You must be logged in to access settings." };
        }
    } catch (e) {
        res.renderWithData('layouts-popout/error', {}, { status: 500, message: e.message });
    }
}
module.exports.changeSetting = async (req, res, next) => {
    try {
        const { setting } = req.body;
        if (req.user) {
            let user = await User.findById(req.user._id);
            if (user && setting && setting.key && setting.value && setting.type) {
                if (setting.type == "toggle") {
                    user.settings[setting.key] = stringToBoolean(setting.value);
                    await user.save();
                }

                res.json({ status: 200, message: "Successfully saved your setting." });
            } else {
                throw { status: 500, message: "An error occurred retrieving your settings." };
            }
        } else {
            throw { status: 403, message: "You must be logged in to access settings." };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}