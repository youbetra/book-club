const { Book } = require('../models/book');
const { Meeting } = require('../models/meeting');
const { Alert } = require('../models/alert');
const { User } = require('../models/user');
const stringToBoolean = require('../utils/stringToBoolean');

module.exports.getMeetings = async (req, res, next) => {
    try {
        let combinedMeetings = [];
        let combinedMeetingsRaw = [];
        if (req.user) {
            let user = await User.findById(req.user._id).populate({
                path: 'clubs',
                populate: {
                    path: 'books',
                    populate: [{
                        path: 'poster club'
                    }, {
                        path: 'meetings',
                        // select: '_id',
                        populate: {
                            path: 'parentBook',
                            populate: {
                                path: 'club'
                            },
                        },
                        match: {
                            'deleted': {
                                $ne: true
                            }
                        }
                    }],
                    match: {
                        'deleted': {
                            $ne: true
                        }
                    }
                },
                match: {
                    'deleted': {
                        $ne: true
                    }
                }
            });

            user.clubs.forEach((club, index) => {
                club.books.forEach((book, index) => {
                    book.meetings.forEach((meeting, index) => {
                        combinedMeetingsRaw.push(meeting);

                        if (meeting.repeat == "weekly") {
                            combinedMeetings.push({
                                "id": meeting._id,
                                "title": `${book.title}: ${meeting.subject}`,
                                "url": `/Books/${book._id}`,
                                daysOfWeek: [meeting.datetime.getDay()],
                                startRecur: meeting.datetime,
                                endRecur: new Date(meeting.datetime.getTime() + (meeting.repeatduration * 604800000)),
                                startTime: meeting.datetime.toLocaleTimeString('it-IT'),
                                endTime: new Date(meeting.datetime.getTime() + 3600000).toLocaleTimeString('it-IT'),
                                color: club.color,
                            });
                        } else {
                            combinedMeetings.push({
                                "id": meeting._id,
                                "title": `${book.title}: ${meeting.subject}`,
                                "url": `/Books/${book._id}`,
                                "start": meeting.datetime, // Milliseconds
                                "end": new Date(meeting.datetime.getTime() + 3600000), // Milliseconds
                                color: club.color,
                            });
                        }
                    });
                });
            });
        }
        res.renderWithData('layouts/meetings', { meetings: combinedMeetingsRaw }, { status: 200, meetings: combinedMeetings });
    } catch (e) {
        res.renderWithData('layouts-popout/error', {}, { status: 500, message: e.message });
    }
}

module.exports.newMeeting = async (req, res, next) => {
    try {
        const data = req.body;

        const bookId = data["newcomment-parentBookId"];
        let parentbook = await Book.findOne({
            _id: bookId,
            deleted: { $ne: true }
        }).populate({
            path: 'club'
        });
        if (parentbook) {
            if (parentbook.club.private) {
                if (req.user) {
                    if (parentbook.club.members.indexOf(req.user._id) == -1) {
                        throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                    }
                } else {
                    throw { status: 403, message: 'This club is private and cannot be accessed without an invitation.' };
                }
            }

            const meeting = new Meeting({
                author: req.user._id,
                body: data["newcomment-body"],
                subject: data["meeting-subject"],
                repeat: data["meeting-repeat"],
                repeatduration: parseInt(data["meeting-repeatduration"]),
                datetime: data["meeting-datetime"],
                parentBook: bookId,
                posted: new Date(),
            });
            await meeting.save();

            const book = await Book.findByIdAndUpdate(bookId,
                { $push: { "meetings": meeting._id }, updated: new Date() },
                { safe: true, upsert: true, new: true }
            ).exec();

            const user = User.findByIdAndUpdate(req.user._id,
                { $push: { "meetings": meeting._id } },
                { safe: true, upsert: true },
                function (err, model) {
                    console.log(err);
                }
            );

            // alerting - this will alert all members of the club when a meeting is posted
            const alertBookLookup = await Book.findOne({
                _id: bookId,
                deleted: { $ne: true }
            }).populate({
                path: 'club',
                populate: 'members'
            });
            alertBookLookup.club.members.forEach(async (member, index) => {
                // (parentbook.club.private) 

                if (member._id.toString() !== req.user._id.toString() && member.settings.alerts.meetings && (alertBookLookup.club.private || member.settings.alertpublic)) {
                    const alert = new Alert({
                        user: member._id,
                        refBook: bookId,
                        refMeeting: meeting._id,
                        refUser: req.user._id,
                        icon: "calendar-event",
                        code: "meeting_new",
                        posted: new Date(),
                    });
                    await alert.save();

                    const alertPoster = await User.findOneAndUpdate({ _id: member._id },
                        { $push: { "alerts": alert._id } },
                        { safe: true, upsert: true, new: true }
                    ).exec();
                }
            });


            res.json({ status: 201, message: "Successfully posted your meeting!", newid: meeting._id });
            // res.json({ status: 201, message: "Successfully posted your comment!", data: { bookid: book.id } });
        } else {
            throw { status: 403, message: "The book you're posting on doesn't appear to exist!" };
        }
    } catch (e) {
        res.json({ status: 500, message: e.message });
    }
}

module.exports.getEditData = async (req, res, next) => {
    try {
        const { meetingId } = req.params;

        const meeting = await Meeting.findOne({
            _id: meetingId,
            deleted: { $ne: true }
        });

        if (meeting) {
            if (req.user._id.toString() != meeting.author.toString()) {
                throw { status: 403, message: 'You cannot edit this meeting.' };
            }

            res.json({
                status: 200, meeting: {
                    type: "Meeting",
                    body: meeting.body,
                    subject: meeting.subject,
                    repeat: meeting.repeat,
                    repeatduration: meeting.repeatduration,
                    datetime: (new Date(meeting.datetime - ((new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0, -1),
                    id: meeting._id
                    // datetime: `${meeting.datetime.getFullYear()}-${meeting.datetime.getMonth() + 1}-${meeting.datetime.getDate()}T${meeting.datetime.getHours()}:${meeting.datetime.getMinutes()}`
                }
            })
        } else {
            throw { status: 404, message: '404 meeting not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.editMeeting = async (req, res, next) => {
    try {
        const data = req.body;
        const { meetingId } = req.params;

        const meeting = await Meeting.findOne({
            _id: meetingId,
            deleted: { $ne: true }
        });

        if (meeting) {
            if (req.user._id.toString() != meeting.author.toString()) {
                throw { status: 403, message: 'You cannot edit this meeting.' };
            }

            meeting.body = data["newcomment-body"];
            meeting.subject = data["meeting-subject"];
            meeting.repeat = data["meeting-repeat"];
            meeting.repeatduration = parseInt(data["meeting-repeatduration"]);
            meeting.datetime = data["meeting-datetime"];
            meeting.updated = new Date()
            await meeting.save();

            res.json({ status: 200, message: 'Successfully edited meeting.', newid: meeting._id });
        } else {
            throw { status: 404, message: '404 meeting not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}

module.exports.deleteMeeting = async (req, res, next) => {
    try {
        const { meetingId } = req.params;

        const meeting = await Meeting.findOne({
            _id: meetingId,
            deleted: { $ne: true }
        });

        if (meeting) {
            if (req.user._id.toString() != meeting.author.toString()) {
                throw { status: 403, message: 'You cannot delete this meeting.' };
            }

            meeting.deleted = true;
            await meeting.save()

            res.json({ status: 200, message: "Successfully deleted the meeting." })
        } else {
            throw { status: 404, message: '404 meeting not found.' };
        }
    } catch (e) {
        console.log(e)
        res.json({ status: 500, message: e.message });
    }
}