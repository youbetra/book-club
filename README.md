# Book Club
[https://bookclub.tetrayoubetra.com/](https://bookclub.tetrayoubetra.com/)

## Description
Book Club was born of my friend group's difficulties planning and hosting book club calls.
Using their complaints and suggestions as a base, like a client project, I've built Book Club to suit their needs.
Additionally, I wanted to make my attempt at a mobiled suited single page application, without any knowledge of React or Angular.

Book Club will let you create a private or public club with which friends can join, post books to discuss, take personal notes, make voting polls, and schedule meeting dates.

### Features
- [ ] Express + EJS backend API
- [ ] Mongo DB
- [ ] Responsive jQuery/DOM frontend App
- [ ] Local and Google oAuth2.0 authentication/signup
- [ ] Cloudinary CDN for user contributed images
- [ ] OpenLibrary API lookups for easy book posting
- [ ] SMTP email sending for user registration and verification
- [ ] Creating clubs
- [ ] Posting books
- [ ] Posting comments
- [ ] Posting notes
- [ ] Posting and voting on polls
- [ ] Posting meeting dates
- [ ] A notification system with dismissable and customizable alerts

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Standard node package - push to host service or download the package and run the package installer.
You will need your environment variables: 
~~~
NODE_ENV=[production/development]
DB_URL=[hostname/path for your Mongo database]
SECRET=[secret key used for sessions and authentication]
PORT=[your host's port]
# Set your cloudinary/API connection information here
CLOUDINARY_CLOUD_NAME=
CLOUDINARY_KEY=
CLOUDINARY_SECRET=
CLONDINARY_FOLDER=
# Email settings
SMTP_FROM=[from address for system emails]
SMTP_USER=[username for email account to send from]
SMTP_PASS=[password for SMTP_USER account below]
SMTP_TEST=[test account for email test mode]
# Set your Google oAuth API settings here
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
~~~

## Support
I do not directly support this application as it is educational in nature. However, if you do have questions about it feel free to reach out to me on [LinkedIn](https://www.linkedin.com/in/tara-wilde-b8959a65/)

## Roadmap
I will likely continue fixing any bugs I might find during testing however I've finished development on my core features.

## Contributing
Feel free to branch and modify this project. Its a self contained Node package, just download it, install the packages, and run it.
Be sure to set the environment variables above.

## Authors and acknowledgment
TBD

## License
This project is for educational purposes. You may use parts of my source code as long as my GitLab profile is linked to in your read me. This may not be used for commercial purposes.

## Project status
Completed
